//
//  AnotherViewController.swift
//  Symptoms
//
//  Created by Sambit Das on 09/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

class SymptomViewController: UIViewController {
    
    @IBOutlet weak var firstView: UIView!
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableview.layer.cornerRadius = 10
        firstView.layer.cornerRadius = 10
//        tableview.rowHeight = UITableView.automaticDimension
//        tableview.estimatedRowHeight = 600

    }
    

  

}
extension SymptomViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SymptomCell
        cell.reloadvar = self
        return cell
        
    }
    
    
    
}
extension SymptomViewController : tableviewreload{
    func reload() {
        tableview.reloadData()
    }
    
    
}
