//
//  SymptomCell.swift
//  Symptoms
//
//  Created by Sambit Das on 09/04/20.
//  Copyright © 2020 Sambit Das. All rights reserved.
//

import UIKit

protocol tableviewreload {
    func reload()
}
class SymptomCell: UITableViewCell {
    @IBOutlet weak var diabeticsBttn: UIButton!
    @IBOutlet weak var bloodPressureBttn: UIButton!
    @IBOutlet weak var feverBttn: UIButton!
    @IBOutlet weak var coldBttn: UIButton!
    @IBOutlet weak var RNoseBttn: UIButton!
    @IBOutlet weak var hostllbl: UILabel!
    @IBOutlet weak var hospitalName: UILabel!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var streetName: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var stateName: UILabel!
    @IBOutlet weak var zipCodeLbl: UILabel!
    @IBOutlet weak var zipCode: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var countryName: UILabel!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var hide : Bool = true
    var reloadvar : tableviewreload?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        diabeticsBttn.layer.cornerRadius = 10
        bloodPressureBttn.layer.cornerRadius = 10
        feverBttn.layer.cornerRadius = 10
        coldBttn.layer.cornerRadius = 10
        RNoseBttn.layer.cornerRadius = 10
    }

    @IBAction func PreExistingSectionHideBttn(_ sender: Any) {
        
        if (hide == true){
            diabeticsBttn.isHidden = true
            bloodPressureBttn.isHidden = true
            hide = false
        }else{
            diabeticsBttn.isHidden = false
            bloodPressureBttn.isHidden = false
            hide = true
            
        }
    }
    @IBAction func SymptomsSectionHideBttn(_ sender: Any) {
        if (hide == true){
                  feverBttn.isHidden = true
                  coldBttn.isHidden = true
                  RNoseBttn.isHidden = true
                   hide = false
               }else{
                   feverBttn.isHidden = false
                   coldBttn.isHidden = false
                   RNoseBttn.isHidden = false
                   hide = true
                   
               }
        }
    @IBAction func DoctorSectionHideBttn(_ sender: Any) {
        if (hide == true){
            hostllbl.isHidden = true
            hospitalName.isHidden = true
            streetLbl.isHidden = true
            streetName.isHidden = true
            cityLbl.isHidden = true
            cityName.isHidden = true
            stateName.isHidden = true
            stateLbl.isHidden = true
            zipCodeLbl.isHidden = true
            zipCode.isHidden = true
            countryLbl.isHidden = true
            countryName.isHidden = true
            hide = false
            tableViewHeight.constant = 630
            reloadvar?.reload()
            
            
     }else{
            hostllbl.isHidden = false
            hospitalName.isHidden = false
            streetLbl.isHidden = false
            streetName.isHidden = false
            cityLbl.isHidden = false
            cityName.isHidden = false
            stateName.isHidden = false
            stateLbl.isHidden = false
            zipCodeLbl.isHidden = false
            zipCode.isHidden = false
            countryLbl.isHidden = false
            countryName.isHidden = false
            hide = true
            tableViewHeight.constant = 780
            reloadvar?.reload()
            
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
